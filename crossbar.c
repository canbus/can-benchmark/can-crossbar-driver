#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/container_of.h>

#define MASK_1BIT 0x1
#define MASK_2BIT 0x3

static const ssize_t reg_size = 11;
static const char* device_name = "ctucancrossbar";

struct crossbar_data {
    void* mem;
    struct miscdevice miscdev;
};

static ssize_t read_bits(struct device* dev, char* buf, size_t off, size_t mask)
{
    struct crossbar_data* data = dev_get_drvdata(dev);
    unsigned int value = ioread32(data->mem);
    return snprintf(buf, PAGE_SIZE, "%d\n", (value >> off) & mask);
}

static ssize_t write_bits(struct device *dev, const char* buf, size_t count, size_t off, size_t mask)
{
    struct crossbar_data* data = dev_get_drvdata(dev);
    unsigned int value = ioread32(data->mem);
    int num = buf[0] - '0';
    dev_info(dev, "Writing %d bits (%d)", count, num);
    if (count < 1) {
        dev_err(dev, "Invalid number of bits to write");
        return 0;
    }
    if (num < 0 || num > 4)
    {
        dev_err(dev, "Invalid value is being written - must be between 0 and 4");
        return 0;
    }
    value &= ~mask << off; //Clear bits
    value |= num << off; //Set new bits
    iowrite32(value, data->mem);
    return count;
}

/* ATTRIBUTES LINE OUTPUT ENABLE*/
/* 1 */
static ssize_t line1_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 24, MASK_1BIT);
}
static ssize_t line1_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 24, MASK_1BIT);
}
static DEVICE_ATTR_RW(line1);
/* 2 */
static ssize_t line2_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 25, MASK_1BIT);
}
static ssize_t line2_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 25, MASK_1BIT);
}
static DEVICE_ATTR_RW(line2);
/* 3 */
static ssize_t line3_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 26, MASK_1BIT);
}
static ssize_t line3_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 26, MASK_1BIT);
}
static DEVICE_ATTR_RW(line3);
/* 4 */
static ssize_t line4_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 27, MASK_1BIT);
}
static ssize_t line4_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 27, MASK_1BIT);
}
static DEVICE_ATTR_RW(line4);

/* ATTRIBUTES BUSn_LINE - which line is the external bus connected to */
/* 1 */
static ssize_t bus1_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 16, MASK_2BIT);
}
static ssize_t bus1_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 16, MASK_2BIT);
}
static DEVICE_ATTR_RW(bus1);
/* 2 */
static ssize_t bus2_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 18, MASK_2BIT);
}
static ssize_t bus2_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 18, MASK_2BIT);
}
static DEVICE_ATTR_RW(bus2);
/* 3 */
static ssize_t bus3_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 20, MASK_2BIT);
}
static ssize_t bus3_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 20, MASK_2BIT);
}
static DEVICE_ATTR_RW(bus3);
/* 4 */
static ssize_t bus4_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 22, MASK_2BIT);
}
static ssize_t bus4_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 22, MASK_2BIT);
}
static DEVICE_ATTR_RW(bus4);

/* ATTRIBUTES CTRLn_LINE - which line is the controller connected to*/
/* 1 */
static ssize_t ctrl1_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 0, MASK_2BIT);
}
static ssize_t ctrl1_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 0, MASK_2BIT);
}
static DEVICE_ATTR_RW(ctrl1);
/* 2 */
static ssize_t ctrl2_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 2, MASK_2BIT);
}
static ssize_t ctrl2_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 2, MASK_2BIT);
}
static DEVICE_ATTR_RW(ctrl2);
/* 3 */
static ssize_t ctrl3_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
    return write_bits(dev, buf, count, 4, MASK_2BIT);
}
static ssize_t ctrl3_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 4, MASK_2BIT);
}
static DEVICE_ATTR_RW(ctrl3);
/* 4 */
static ssize_t ctrl4_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 6, MASK_2BIT);
}
static ssize_t ctrl4_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 6, MASK_2BIT);
}
static DEVICE_ATTR_RW(ctrl4);
/* 5 */
static ssize_t ctrl5_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 8, MASK_2BIT);
}
static ssize_t ctrl5_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 8, MASK_2BIT);
}
static DEVICE_ATTR_RW(ctrl5);
/* 6 */
static ssize_t ctrl6_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 10, MASK_2BIT);
}
static ssize_t ctrl6_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 10, MASK_2BIT);
}
static DEVICE_ATTR_RW(ctrl6);
/* 7 */
static ssize_t ctrl7_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 12, MASK_2BIT);
}
static ssize_t ctrl7_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 12, MASK_2BIT);
}
static DEVICE_ATTR_RW(ctrl7);
/* 8 */
static ssize_t ctrl8_store(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	return write_bits(dev, buf, count, 14, MASK_2BIT);
}
static ssize_t ctrl8_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return read_bits(dev, buf, 14, MASK_2BIT);
}
static DEVICE_ATTR_RW(ctrl8);

static struct attribute *crossbar_attrs[] = {
    &dev_attr_line1.attr,
    &dev_attr_line2.attr,
    &dev_attr_line3.attr,
    &dev_attr_line4.attr,
    &dev_attr_bus1.attr,
    &dev_attr_bus2.attr,
    &dev_attr_bus3.attr,
    &dev_attr_bus4.attr,
    &dev_attr_ctrl1.attr,
    &dev_attr_ctrl2.attr,
    &dev_attr_ctrl3.attr,
    &dev_attr_ctrl4.attr,
    &dev_attr_ctrl5.attr,
    &dev_attr_ctrl6.attr,
    &dev_attr_ctrl7.attr,
    &dev_attr_ctrl8.attr,
    NULL
};
static const struct attribute_group crossbar_attrs_group = {
    .name = "reg",
    .attrs = crossbar_attrs,
};
static const struct attribute_group *crossbar_attrs_groups[] = {
    &crossbar_attrs_group,
    NULL
};

static ssize_t device_file_read(struct file *f, char __user *user_buffer, size_t count, loff_t *position)
{
    char buf[12];
    struct crossbar_data* priv = (struct crossbar_data*)f->private_data;
    unsigned int data;
    printk(KERN_NOTICE "crossbar: Read at %d, bytes count = %u\n", (int)*position, (unsigned int)count);
    data = ioread32(priv->mem);
    snprintf(buf, 12, "0x%08X\n", data);

    if (*position >= reg_size)
        return 0;

    if (*position + count > reg_size)
        count = reg_size - *position;

    if (copy_to_user(user_buffer, buf + *position, count) != 0)
        return -EFAULT;

    *position += count;
    return count;
}

static int dev_file_open(struct inode *inode, struct file *f)
{
    // Make private data point to the whole data structure, not just misc device (axis-fifo does this too)
    struct crossbar_data *data;
    pr_info("crossbar: File Open\n");
    data = container_of(f->private_data, struct crossbar_data, miscdev);
    f->private_data = data;
    return 0;
}

static int dev_file_release(struct inode *inode, struct file *file)
{
    pr_info("crossbar: File Release\n");
    return 0;
}

/*
** This function will be called when we write the Device file
*/
static ssize_t dev_file_write(struct file *f, const char __user *ubuf, size_t ulen, loff_t *off)
{
    struct crossbar_data* priv = (struct crossbar_data*)f->private_data;
    int data;
    int len;
    char buf[11+2]; //Incl null from buff + added one
    len = ulen > reg_size+1 ? reg_size+1 : ulen;

    if (copy_from_user(buf, ubuf, len) != 0)
    {
        pr_warn("crossbar: Could not copy user buf\n");
        return 0;
    }
    buf[len] = '\0';//Make sure to null terminate

    pr_info("crossbar: Write %d chars at %lld\n", len, *off);
    if (*off != 0)
    {
        pr_warn("crossbar: Write at offset unsupported\n");
        return 0;
    }

    if (sscanf(buf, "0x%X", &data) != 1)
    {
        pr_warn("crossbar: Write in invalid format\n");
        return 0;
    }
    iowrite32(data, priv->mem);

    return len;
}

static const struct file_operations crossbar_fops = 
{
    .owner = THIS_MODULE,
    .read = device_file_read,
    .write = dev_file_write,
    .open = dev_file_open,
    .release = dev_file_release,
};

static int cb_probe(struct platform_device *pdev)
{
    int result = 0;
    struct resource* res;
    struct crossbar_data* data = NULL;
    printk(KERN_NOTICE "crossbar: Initializing\n");

    /* Allocate private structure */
    data = devm_kzalloc(&pdev->dev, sizeof(*data), GFP_KERNEL);
    if (!data)
        return -ENOMEM;
    dev_set_drvdata(&pdev->dev, data);

    /* Create misc device */
    data->miscdev.fops = &crossbar_fops;
    data->miscdev.minor = MISC_DYNAMIC_MINOR;
    data->miscdev.name = device_name;
    //data->miscdev.groups = axis_fifo_attrs_groups;
    data->miscdev.parent = &pdev->dev;
    result = misc_register(&data->miscdev);
    if (result < 0)
    {
        pr_warn("crossbar: Could not register device - error code = %d\n", result);
        return -1;
    }

    result = devm_device_add_groups(&pdev->dev, crossbar_attrs_groups);
    if (result < 0)
    {
        pr_warn("crossbar: Could not add attribute groups - error code = %d\n", result);
        return -1;
    }

    res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
    data->mem = devm_ioremap_resource(&pdev->dev, res);
    if (IS_ERR(data->mem)) {
        dev_err(&pdev->dev, "Cannot remap address\n");
        return PTR_ERR(data->mem);
    }

    printk(KERN_NOTICE "crossbar: seems good\n");
    return 0;
}
    
static int cb_remove(struct platform_device *pdev)
{
    struct crossbar_data *data = dev_get_drvdata(&pdev->dev);
    printk(KERN_NOTICE "crossbar: Exiting\n");
    misc_deregister(&data->miscdev);
    return 0;
}
/* Match table for OF platform binding */
static const struct of_device_id of_match[] = {
	{ .compatible = "can-crossbar", },
    { .compatible = "ctu,can-crossbar", },
	{ /* end of list */ },
};
MODULE_DEVICE_TABLE(of, of_match);

static struct platform_driver crossbar_driver = {
	.probe	= cb_probe,
	.remove	= cb_remove,
	.driver	= {
		.name = "ctu-can-crossbar",
		//.pm = &ctucan_platform_pm_ops,
		.of_match_table	= of_match,
	},
};

module_platform_driver(crossbar_driver);
MODULE_LICENSE("GPL");
